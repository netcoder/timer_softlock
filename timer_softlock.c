#include <linux/workqueue.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/slab.h>

MODULE_LICENSE("GPL");

#define MY_TIMERS 1000
#define REARMED_MAX 1000

struct my_timer_work {
	struct timer_list timer;
	int rearmed;
};

static struct my_timer_work *timers;

static void my_timer_expire(struct timer_list *timer)
{
	struct my_timer_work *t = from_timer(t, timer, timer);

	mdelay(10);

	if (t->rearmed < REARMED_MAX) {
		mod_timer(timer, jiffies + msecs_to_jiffies(100));
		t->rearmed++;
	}
}

static void __exit lock_bursttest_exit(void)
{
	int i;

	for (i = 0; i < MY_TIMERS; i++) {
		timer_shutdown_sync(&timers[i].timer);
	}

	kfree(timers);
}

static int __init lock_bursttest_init(void)
{
	int i;

	timers = kzalloc(sizeof(*timers) * MY_TIMERS, GFP_KERNEL);
	if (!timers)
		return -ENOMEM;

	for (i = 0; i < MY_TIMERS; i++) {
		timer_setup(&timers[i].timer, my_timer_expire, 0);
		mod_timer(&timers[i].timer, jiffies + msecs_to_jiffies(100));
	}

	return 0;
}

module_init(lock_bursttest_init);
module_exit(lock_bursttest_exit);
